jQuery( function( $ ) {

	var $select   = $( '#fit-press-exercise-selector' ),
		workout,
		exerciseSelector;


	var Set = Backbone.Model.extend({
		defaults: {
			weight: '',
			reps  : ''
		}
	});


	var SetView = Backbone.View.extend({
		tagName: 'li',
		initialize: function( args ) {
			this.exerciseId = args.exerciseId;

			this.render( args.index );
		},
		render: function( index ) {
			var template = _.template( $( '#set-template').html(), {
				model     : this.model.toJSON(),
				index     : index,
				exerciseId: this.exerciseId
			});

			this.$el.html( template );

			return this;
		},
		events: {
			'keyup input.fit-press-weight': 'updateWeight',
			'keyup input.fit-press-reps'  : 'updateReps',
			'click a.remove-set'          : 'removeSet'
		},
		updateWeight: function( e ) {
			this.model.set( 'weight', +e.target.value );
		},
		updateReps: function( e ) {
			this.model.set( 'reps', +e.target.value );
		},
		removeSet: function( e ) {
			e.preventDefault();
			this.model.destroy();
		}
	});


	var Sets = Backbone.Collection.extend({
		model: Set,
		initialize: function() {
			this.add( {} );
		}
	});


	var SetsView = Backbone.View.extend({
		tagName: 'ul',
		initialize: function( args ) {
			this.exerciseId   = args.exerciseId;
			this.exerciseName = args.exerciseName;

			_.bindAll( this, 'render' );

			this.render();

			this.collection.bind( 'add',    this.renderSets, this );
			this.collection.bind( 'remove', this.renderSets, this );
		},
		events: {
			'click a.remove-exercise': 'removeExercise',
			'click a.add-set'        : 'addSet',
			'click a.copy-set'       : 'copySet',
			'click a.new-exercise'   : 'newExercise'
		},
		removeExercise: function( e ) {
			e.preventDefault();
			$( 'li' ).find( "[data-id='" + this.exerciseId + "']" ).removeClass( 'exercise-selected' );
			var exercise = workout.collection.where({ exerciseId: this.exerciseId });
			workout.collection.remove( exercise );
		},
		addSet: function( e ) {
			e.preventDefault();
			this.collection.add({});

			$( '.fit-press-weight:last', this.$el ).focus().trigger( 'click' );
		},
		copySet: function( e ) {
			e.preventDefault();
			var lastSet = this.collection.at( this.collection.length - 1 );
			this.collection.add( lastSet.toJSON() );

			$( '.fit-press-reps:last', this.$el ).focus().trigger( 'click' );
		},
		newExercise: function( e ) {
			e.preventDefault();
			var top = $( '#fit-press-area-selector' ).offset().top - 50;
			$( 'body' ).animate({ scrollTop: top }, 300 );
		},
		render: function() {
			var template = _.template( $( '#sets-template').html(), { exerciseName: this.exerciseName } );

			this.$el.html( template );
			this.renderSets();

			return this;
		},
		renderSets: function() {
			var setsContainer = this.$( '#sets-container' ),
				self          = this;

			setsContainer.empty();

			if ( 0 === this.collection.length ) {
				$( 'a.copy-set', this.$el ).addClass( 'hide-copy' );
			} else {
				this.collection.each( function( set, index ) {
					var setView = new SetView({
						model     : set,
						index     : index,
						exerciseId: self.exerciseId
					});

					setsContainer.append( setView.$el );
				});
				$( 'a.copy-set', this.$el ).removeClass( 'hide-copy' );
			}

			return this;
		}
	});


	var WorkoutExercise = Backbone.Model.extend({
		defaults: {
			exerciseId  : '',
			exerciseName: '',
			sets        : ''
		}
	});


	var WorkoutExercises = Backbone.Collection.extend({
		model: WorkoutExercise
	});


	var WorkoutView = Backbone.View.extend({
		el: '#fit-press-container',
		initialize: function() {
			_.bindAll( this, 'render' );

			this.render();

			this.collection.bind( 'add',    this.render, this );
			this.collection.bind( 'remove', this.render, this );
		},
		render: function() {
			var self = this;
			this.$el.empty();

			this.collection.each( function( model, index ) {
				var exerciseView = new SetsView({
					collection  : model.get( 'sets' ),
					exerciseId  : model.get( 'exerciseId' ),
					exerciseName: model.get( 'exerciseName' )
				});

				self.$el.append( exerciseView.$el );
			});

			$( '.fit-press-weight:last', this.$el ).focus().trigger( 'click' );

		}
	});


	/** Exercises **/

	var Exercise = Backbone.Model.extend({
		defaults: {
			id  : '',
			name: '',
			area: []
		}
	});

	var Exercises = Backbone.Collection.extend({
		model: Exercise,
		byArea: function( area ) {
			filtered = this.filter( function( exercise ) {
				return _.indexOf( exercise.get( 'area' ), area ) > -1;
			});
			return new Exercises( filtered );
		},
		byAreas: function( areas ) {
			if ( 0 === areas.length ) {
				return new Exercises( this.models );
			}

			filtered = this.filter( function( exercise ) {
				var exerciseAreas = exercise.get( 'area' ),
					i;

				for ( i = 0; i < exerciseAreas.length; i++ ) {
					if ( _.indexOf( areas, exerciseAreas[i] ) > -1 ) {
						return true;
					}
				}

				return false;
			});
			return new Exercises( filtered );
		}

	});

	var ExerciseArea = Backbone.Model.extend({
		defaults: {
			name: ''
		}
	});

	var ExerciseAreas = Backbone.Collection.extend({
		model: ExerciseArea
	});


	var ExerciseAreasView = Backbone.View.extend({
		el            : '#fit-press-exercise-select',
		$areaSelectors: [],
		areas         : [],
		loading       : false,
		initialize: function( args ) {
			this.areas = args.areas;
			this.renderAreas();
			this.renderExercises( this.collection );

			_.bindAll( this, 'renderExercises' );

			this.$areaSelectors = $( '#fit-press-area-selector a' );
		},
		events: {
			'click #fit-press-area-selector a.exercise'  : 'filterExercise',
			'click #fit-press-area-selector a#create-new': 'newExercise',
			'click #fit-press-exercises a'               : 'addExercise',
			'click a#fit-press-new-exercise-submit'      : 'createExercise',
			'click a#fit-press-new-exercise-cancel'      : 'cancelCreateExercise'
		},
		filterExercise: function( e ) {
			e.preventDefault();

			var $this = $( e.target ),
				exercises;

			if ( $this.hasClass( 'active' ) ) {
				$this.removeClass( 'active' );
				if ( $this.hasClass( 'hide-all' ) ) {
					$this.text( fitPress.hideAll );
				}
				exercises = this.collection;

			} else {
				$( '#fit-press-area-selector a' ).removeClass( 'active' );
				$this.addClass( 'active' );

				if ( $this.hasClass( 'hide-all' ) ) {
					$this.text( fitPress.showAll );
				}

				var area  = $this.data( 'area' );
				exercises = this.collection.byArea( area );

			}

			this.renderExercises( exercises );
		},
		filterExercises: function( e ) {
			e.preventDefault();

			var filters = [],
				exercises;

			$( e.target ).toggleClass( 'active' );

			$.each( this.$areaSelectors, function() {
				if ( $( this ).hasClass( 'active' ) ) {
					filters.push( $( this ).data( 'area' ) );
				}
			});

			exercises = this.collection.byAreas( filters );

			this.renderExercises( exercises );
		},
		newExercise: function( e ) {
			e.preventDefault();

			$areaSelect = this.$( 'ul#body-part-terms-list' );
			this.$( '#fit-press-new-exercise-container' ).fadeIn( 300 );

			if ( 0 === $areaSelect.children().length ) {
				this.areas.each( function( model, index ) {
					var template = _.template(
						'<li><input type="checkbox" name="fit-press-new-exercise-checkbox" value="<%= val %>" id="<%= id %>" /><label for="<%= id %>"><%= area %></label>',
						{
							area: model.get( 'name' ),
							id  : 'fpacheck-' + index,
							val : model.get( 'term_id' )
						}
					);
					$areaSelect.append( template );
				});
			}

		},
		createExercise: function( e ) {
			e.preventDefault();

			if ( true === this.loading ) {
				return;
			}

			var $title  = $( '#fit-press-new-exercise' ),
				$inputs = $( 'input:checked', this.$( 'ul#body-part-terms-list' ) ),
				termIDs = [],
				self    = this;

			$.each( $inputs, function( i ) {
				termIDs.push( +$( this ).val() );
			});

			if ( 0 === termIDs.length ) {
				alert( 'Please select a category for the exercise' );
				return;
			}

			this.loading = true;
			$( '#new-exercise-spinner' ).show();
			var data = {
					action  : 'fit-press-create-exercise',
					title   : $title.val(),
					term_ids: termIDs,
					security: fitPress.ajaxNonce
				};

 			$.post( ajaxurl, data ).done( function( result ) {
				if ( true === result.success ) {

					$( '#fit-press-area-selector a' ).removeClass( 'active' );

					self.collection.add( result.data, self.collection );
					self.collection.sort();
					self.renderExercises( self.collection );

					self.$( '#fit-press-new-exercise-container' ).fadeOut( 200, function() {
						$title.val( '' );
						$inputs.attr( 'checked', false );
					});

				} else {
					alert( 'Unable to create exercise!' );

				}

			}).always( function( result ) {
				self.loading = false;
				$( '#new-exercise-spinner' ).hide();

			});

		},
		cancelCreateExercise: function( e ) {
			e.preventDefault();
			var self = this;
			this.$( '#fit-press-new-exercise-container' ).fadeOut( 200, function() {
				$( '#fit-press-new-exercise' ).val( '' );
				$( 'input:checked', self.$( 'ul#body-part-terms-list' ) ).attr( 'checked', false );
			});
		},
		addExercise: function( e ) {
			e.preventDefault();

			var $link = $( e.target );
			if ( $link.hasClass( 'exercise-selected' ) ) return;

			$link.addClass( 'exercise-selected' );

			workout.collection.add({
				exerciseId  : $link.data( 'id' ),
				exerciseName: $link.text(),
				sets        : new Sets()
			});

			this.autosaveWorkout();
		},
		renderAreas: function() {
			var $areas = this.$( '#fit-press-area-selector' );

			$areas.append( '<a class="button exercise hide-all" data-area="0" href="#">' + fitPress.hideAll + '</a>' );

			this.areas.each( function( model, index ) {
				var template = _.template(
					'<a class="button exercise" href="#" data-area="<%= area %>"><%= area %></a>',
					{ area: model.get( 'name' ), id: 'fpas-' + index }
				);
				$areas.append( template );
			});

			$areas.append( '<a class="button" id="create-new" href="#">' + fitPress.newExercise + '</a>' );
		},
		renderExercises: function( exercises ) {
			var $exercises = this.$( '#fit-press-exercises' );
			$exercises.empty();

			exercises.each( function( model, index ) {
				var template = _.template(
					'<li><a href="#" data-id="<%= id %>"><%= name %></a></li>', model.toJSON()
				);
				$exercises.append( template );
			});
		},
		getWorkoutAsObject: function() {
			var workoutExercises = {};

			workout.collection.each( function( model ) {
				var id = model.get( 'exerciseId' );
				workoutExercises[ id ] = [];

				var sets = model.get( 'sets' );
				sets.each( function( set ) {
					workoutExercises[ id ].push( set.toJSON() );
				});

			});

			return workoutExercises;
		},
		autosaveWorkout: function() {
			if ( true === this.saving ) {
				return;
			}

			this.saving = true;

			var workoutExercises = this.getWorkoutAsObject(),
				self = this;

			if ( $.isEmptyObject( workoutExercises ) ) {
				return;
			}

			var data = {
				action   : 'fit-press-autosave-workout',
				exercises: workoutExercises,
				id       : this.$el.data( 'post-id' ),
				security : fitPress.ajaxNonce
			};

			$.post( ajaxurl, data ).done( function( result ) {
				console.log( result );
				self.saving = false;

			});
		}

	});


	workout = new WorkoutView({
		collection: new WorkoutExercises()
	});

	exerciseSelector = new ExerciseAreasView({
		collection: new Exercises( fitPress.exercises ),
		areas     : new ExerciseAreas( fitPress.areas )
	});

	exerciseSelector.collection.comparator = 'name';

	if ( fitPress.workout.length > 0 ) {
		var i;
		for ( i = 0; i < fitPress.workout.length; i++ ) {
			var curr = fitPress.workout[i];
			workout.collection.add({
				exerciseId  : curr.exercise.ID,
				exerciseName: curr.exercise.post_title,
				sets        : new Sets( curr.sets )
			});
		}
	}

});
