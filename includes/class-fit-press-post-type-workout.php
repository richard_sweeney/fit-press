<?php

if ( ! defined( 'ABSPATH' ) ) return;

class Fit_Press_Post_Type_Workout {

	public static $instance;

	public function __construct() {
		$hook_factory = new Fit_Press_Filter_Action_Factory( $this );
		$hook_factory->register_actions( $this->get_actions() );
		$hook_factory->register_filters( $this->get_filters() );
	}


	public function get_actions() {
		return array(
			array(
				'hook'     => 'init',
				'callback' => 'register_post_type',
			),
			array(
				'hook'     => 'edit_form_after_title',
				'callback' => 'add_new_workout_fields',
			),
			array(
				'hook'     => 'post_edit_form_tag',
				'callback' => 'do_not_validate_html5_forms',
			),
			array(
				'hook'     => 'manage_workout_posts_custom_column',
				'callback' => 'manage_workout_columns',
				'priority' => 10,
				'num_args' => 2
			),
		);
	}


	public function get_filters() {
		return array(
			array(
				'hook'     => 'post_updated_messages',
				'callback' => 'edit_updated_messages',
			),
			array(
				'hook'     => 'manage_edit-workout_columns',
				'callback' => 'add_workout_admin_columns',
			),
		);
	}


	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.0.1
	 *
	 * @return A single instance of this class.
	 */
	public static function get() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}


	/** Register the post types. */
	public function register_post_type() {

		register_post_type( 'workout', array(
			'hierarchical'      => false,
			'public'            => true,
			'show_ui'           => true,
			'supports'          => array( 'title' ),
			'has_archive'       => true,
			'query_var'         => true,
			'rewrite'           => true,
			'labels'            => array(
				'name'               => __( 'Workouts', 'fit_press' ),
				'singular_name'      => __( 'Workout', 'fit_press' ),
				'all_items'          => __( 'Workouts', 'fit_press' ),
				'new_item'           => __( 'New workout', 'fit_press' ),
				'add_new'            => __( 'Add New', 'fit_press' ),
				'add_new_item'       => __( 'Add New workout', 'fit_press' ),
				'edit_item'          => __( 'Edit workout', 'fit_press' ),
				'view_item'          => __( 'View workout', 'fit_press' ),
				'search_items'       => __( 'Search workouts', 'fit_press' ),
				'not_found'          => __( 'No workouts found', 'fit_press' ),
				'not_found_in_trash' => __( 'No workouts found in trash', 'fit_press' ),
				'parent_item_colon'  => __( 'Parent workout', 'fit_press' ),
				'menu_name'          => __( 'Workouts', 'fit_press' ),
			),
		) );

	}


	/**
	 * Add backbone stuff to add exercises to a workout.
	 *
	 * @param object $post WP_Post object.
	 */
	public function add_new_workout_fields( $post ) {
		if ( 'workout' !== get_post_type( $post ) ) {
			return;
		}

		include_once FITPRESS_DIR . 'views/admin-new-workout.php';
	}



	/**
	 * Edit the updated messages for the exercises types.
	 *
	 * @param array $messages Array of post update messages.
	 *
	 * @return array Array of post update messages.
	 */
	public function edit_updated_messages( $messages ) {
		global $post;

		$permalink = get_permalink( $post );

		$messages['workout'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __('Workout updated. <a target="_blank" href="%s">View workout</a>', 'fit_press'), esc_url( $permalink ) ),
			2 => __('Custom field updated.', 'fit_press'),
			3 => __('Custom field deleted.', 'fit_press'),
			4 => __('Workout updated.', 'fit_press'),
			/* translators: %s: date and time of the revision */
			5 => isset($_GET['revision']) ? sprintf( __('Workout restored to revision from %s', 'fit_press'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __('Workout published. <a href="%s">View workout</a>', 'fit_press'), esc_url( $permalink ) ),
			7 => __('Workout saved.', 'fit_press'),
			8 => sprintf( __('Workout submitted. <a target="_blank" href="%s">Preview workout</a>', 'fit_press'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
			9 => sprintf( __('Workout scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview workout</a>', 'fit_press'),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
			10 => sprintf( __('Workout draft updated. <a target="_blank" href="%s">Preview workout</a>', 'fit_press'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		);

		return $messages;
	}


	/**
	 * Add additional columns to the exercise admin tables.
	 *
	 * @param array $columns Array of registered columns for the admin tables.
	 *
	 * @return array Updated columns array.
	 */
	public function add_workout_admin_columns( $columns ) {
		return array(
			'cb'           => '<input type="checkbox" />',
			'title'        => __( 'Title', 'fit_press' ),
			'exercises'    => __( 'Exercises', 'fit_press' ),
			'workout_date' => __( 'Date & Time', 'fit_press' ),
		);
	}


	/**
	 * Callback to render the content for the custom admin columns/
	 *
	 * @param string $column  Column ID.
	 * @param int    $post_id Post ID.
	 */
	public function manage_workout_columns( $column, $post_id ) {
		global $post;

		switch( $column ) {

			case 'exercises' :

				$exercises = fit_press_get_workout_exercises( $post_id );
				$exercise_names = array();
				if ( $exercises ) {
					foreach ( $exercises as $ex ) {
						$exercise_names[] = sprintf( '<a href="%s">%s</a>', get_the_permalink( $ex->exercise->ID ), $ex->exercise->post_title );
					}
					echo implode( ', ', $exercise_names );
				}
				break;


			case 'workout_date' :
				echo get_the_time( 'Y-m-d H:i', $post_id );
				break;

		}

	}


	/**
	 * Add the novalidate attribute to the workout form.
	 *
	 * @param Object $post WP_Post object.
	 */
	public function do_not_validate_html5_forms( $post ) {
		if ( 'workout' != get_post_type( $post ) ) {
			return;
		}
		echo ' novalidate';
	}

}

