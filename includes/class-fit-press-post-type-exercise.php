<?php

if ( ! defined( 'ABSPATH' ) ) return;

class Fit_Press_Post_Type_Exercise {

	public static $instance;

	public function __construct() {
		$hook_factory = new Fit_Press_Filter_Action_Factory( $this );
		$hook_factory->register_actions( $this->get_actions() );
		$hook_factory->register_filters( $this->get_filters() );
	}


	public function get_actions() {
		return array(
			array(
				'hook'     => 'init',
				'callback' => 'register_post_type',
			),
			array(
				'hook'     => 'init',
				'callback' => 'register_taxonomies',
			),
			array(
				'hook'     => 'pre_get_posts',
				'callback' => 'change_exercise_listing_order',
			),
			array(
				'hook'     => 'restrict_manage_posts',
				'callback' => 'add_exercise_taxonomy_filters',
			),
		);
	}


	public function get_filters() {
		return array(
			array(
				'hook'     => 'post_updated_messages',
				'callback' => 'edit_updated_messages',
			),
		);
	}


	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.0.1
	 *
	 * @return A single instance of this class.
	 */
	public static function get() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}


	/** Register the post types. */
	public function register_post_type() {
		register_post_type( 'exercise', array(
			'hierarchical'      => false,
			'public'            => true,
			'show_in_menu'      => 'edit.php?post_type=workout',
			'supports'          => array( 'title', 'editor', 'thumbnail' ),
			'has_archive'       => true,
			'query_var'         => true,
			'rewrite'           => true,
			'labels'            => array(
				'name'               => __( 'Exercises', 'fit_press' ),
				'singular_name'      => __( 'Exercise', 'fit_press' ),
				'all_items'          => __( 'Exercises', 'fit_press' ),
				'new_item'           => __( 'New exercise', 'fit_press' ),
				'add_new'            => __( 'Add New', 'fit_press' ),
				'add_new_item'       => __( 'Add New exercise', 'fit_press' ),
				'edit_item'          => __( 'Edit exercise', 'fit_press' ),
				'view_item'          => __( 'View exercise', 'fit_press' ),
				'search_items'       => __( 'Search exercises', 'fit_press' ),
				'not_found'          => __( 'No exercises found', 'fit_press' ),
				'not_found_in_trash' => __( 'No exercises found in trash', 'fit_press' ),
				'parent_item_colon'  => __( 'Parent exercise', 'fit_press' ),
				'menu_name'          => __( 'Exercises', 'fit_press' ),
			),
		) );

	}


	/**
	 * Register custom taxonomies.
	 */
	public function register_taxonomies() {
	    register_taxonomy( 'body_part', 'exercise', array(
            'label'             => __( 'Body part', 'fit_press' ),
            'rewrite'           => array( 'slug' => 'body-part' ),
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_nav_menus' => false,
			'show_in_menu'      => 'admin.php?page=fit-press-admin',
        ) );

	}


	/**
	 * Order exercises by title (not published date).
	 *
	 * @param object $query WP_Query obejct.
	 */
	public function change_exercise_listing_order( $query ) {
		if ( 'exercise' === $query->query['post_type'] && $query->is_main_query() ) {
			$query->set( 'orderby', 'title' );
			$query->set( 'order', 'asc' );
		}
	}


	/**
	 * Filter the output of the permalink stuff for exercises.
	 *
	 * @param string     $return    The HTML of the sample permalink slug editor.
	 * @param int|object $id        Post ID or post object.
 	 * @param string     $new_title Optional. New title.
 	 * @param string     $new_slug  Optional. New slug.
 	 *
 	 * @return string The HTML of the sample permalink slug editor.
 	 */
	public function filter_exercise_permalink( $return, $id, $new_title, $new_slug ) {
		if ( 'exercise' !== get_post_type( $id ) ) {
			return $return;
		}

		$return = sprintf( '<strong>%1$s</strong>: %2$s <span id="view-post-btn"><a class="button button-small" href="%2$s">%3$s</a></span> ', __( 'Permalink', 'fit_press' ), home_url( "/all-exercises/?id=$id" ), __( 'View exercise', 'fit_press' ) );

		return $return;
	}



	/**
	 * Edit the updated messages for the exercises types.
	 *
	 * @param array $messages Array of post update messages.
	 *
	 * @return array Array of post update messages.
	 */
	public function edit_updated_messages( $messages ) {
		global $post;

		$permalink = get_permalink( $post );

		$messages['exercise'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __('Exercise updated. <a target="_blank" href="%s">View exercise</a>', 'fit_press'), esc_url( $permalink ) ),
			2 => __('Custom field updated.', 'fit_press'),
			3 => __('Custom field deleted.', 'fit_press'),
			4 => __('Exercise updated.', 'fit_press'),
			/* translators: %s: date and time of the revision */
			5 => isset($_GET['revision']) ? sprintf( __('Exercise restored to revision from %s', 'fit_press'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __('Exercise published. <a href="%s">View exercise</a>', 'fit_press'), esc_url( $permalink ) ),
			7 => __('Exercise saved.', 'fit_press'),
			8 => sprintf( __('Exercise submitted. <a target="_blank" href="%s">Preview exercise</a>', 'fit_press'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
			9 => sprintf( __('Exercise scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview exercise</a>', 'fit_press'),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
			10 => sprintf( __('Exercise draft updated. <a target="_blank" href="%s">Preview exercise</a>', 'fit_press'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		);

		return $messages;
	}


	/**
	 * Add a taxonomy filter to the exercises list.
	 */
	public function add_exercise_taxonomy_filters() {
		global $typenow;

		if ( 'exercise' !== $typenow ) return;

		$selected = get_query_var( 'body_part' );
		$tax      = 'body_part';
		$areas    = get_terms( $tax ); ?>

		<select name="<?php echo $tax ?>" id="<?php echo $tax ?>">
			<option value="0"><?php _e( 'All areas', 'fit_press' ) ?></option>
			<?php foreach ( $areas as $area ) : ?>
				<option <?php selected( $selected, $area->slug ) ?> value="<?php echo esc_attr( $area->slug ) ?>">
					<?php echo esc_html( $area->name ) ?>
				</option>
			<?php endforeach; ?>
		</select>

	<?php }

}
