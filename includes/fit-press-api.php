<?php

if ( ! defined( 'ABSPATH' ) ) return;

/**
 * Save a workout to the DB.
 *
 * @param array $exercises Array of workout exercises.
 * @param int   $post_ID Post ID.
 * @param bool  $update  Whether this is an existing post being updated or not.
 *
 * @return WP_Error|int WP_Error instance or mysql insert id of the workout.
 */
function fit_press_save_workout( $exercises, $post_id = null, $update = false ) {
	global $post;

	$fpdb = Fit_Press_DB::get();

	if ( empty( $exercises ) ) {
		return false;
	}

	if ( null == $post_id ) {
		$post_id = $post->ID;
	}

	if ( $update ) {
		$fpdb->delete_workout_exercises( $post_id );
	}

	$fpdb->set_workout_exercises( $post_id, $exercises );
	return $post_id;
}


/**
 * Delete a workout and all related exercise data.
 *
 * @param int $workout_id post_id.
 *
 * @return bool true on success otherwise false .
 */
function fit_press_delete_workout( $workout_id ) {
	$delete_post = wp_trash_post( $workout_id );
	if ( ! $delete_post ) {
		return false;
	}

	fit_press_delete_workout_exercises( $workout_id );

	return true;
}


/**
 * Get all workouts.
 *
 * @param array $args Array of args to override default argument.
 * @see http://codex.wordpress.org/Class_Reference/WP_Query.
 *
 * @return WP_Query WP_Query object.
 */
function fit_press_get_workouts( $args = array() ) {
	$args = wp_parse_args( $args, array(
		'post_type'      => 'workout',
		'posts_per_page' => -1,
	) );
	return new WP_Query( $args );
}


/**
 * Get all exercises.
 *
 * @param array $args Array of args to override default argument.
 * @see http://codex.wordpress.org/Class_Reference/WP_Query
 *
 * @return array Array of exercises.
 */
function fit_press_get_exercises( $args = array() ) {
	$args = wp_parse_args( $args, array(
		'post_type'      => 'exercise',
		'posts_per_page' => -1,
		'orderby'        => 'title',
		'order'          => 'ASC',
	) );
	$exercises = get_posts( $args );

	if ( empty( $exercises ) ) {
		return false;
	}

	return $exercises;
}


/**
  * Get an exercise by ID.
  *
  * @see get_post().
  */
function fit_press_get_exercise( $id ) {
	return get_post( $id );
}


/**
 * Get all instances of an exercise.
 *
 * @param string|int $id Exercise ID.
 *
 * @return array An array of exercise instances.
 */
function fit_press_get_instances_of_exercise( $id ) {
	$fpdb = Fit_Press_DB::get();

	return $fpdb->get_all_exercise_instances( $id );
}


/**
 * Retrieve a workout by ID.
 *
 * @param int $workout_id post_id.
 *
 * @return StdClass Workout object.
 */
function fit_press_get_workout( $workout_id ) {
	return new Fit_Press_Workout( $workout_id );
}


/**
 * Retrieve all exercises for a workout.
 *
 * @param int  $workout_id post_id.
 * @param bool $copy       Whether or not the workout is to be copied.
 *
 * @return array An array of exercises.
 */
function fit_press_get_workout_exercises( $workout_id = null, $copy = false ) {
	global $post;
	$fpdb = Fit_Press_DB::get();

	if ( null === $workout_id ) {
		$workout_id = $post->ID;
	}

	return $fpdb->get_workout_exercises( $workout_id, $copy );
}


/**
 * Delete all exercises for a workout.
 *
 * @param int  $workout_id post_id.
 *
 * @return bool True on success or false;
 */
function fit_press_delete_workout_exercises( $workout_id ) {
	$fpdb = Fit_Press_DB::get();

	return $fpdb->delete_workout_exercises( $workout_id );
}


/**
 * Retrieve all exercise IDs for a workout.
 *
 * @param int $workout_id post_id.
 *
 * @return array An array of exercise ids.
 */
function fit_press_get_workout_exercises_ids( $workout_id ) {
	$fpdb = Fit_Press_DB::get();

	return $fpdb->get_workout_exercises_ids( $workout_id );
}
