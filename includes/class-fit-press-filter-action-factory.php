<?php

if ( ! defined( 'ABSPATH' ) ) return;

/**
 * Helper class to attach action and filter hooks
 * to objects in WordPress.
 */
class Fit_Press_Filter_Action_Factory {

	/**
	 * @var null|object.
	 */
	private $object = null;


	/**
	 * Constructor function/
	 *
	 * @param object The object that will subscribe to the action & filter hooks.
	 */
	public function __construct( $object = null ) {
		$this->object = $object;
	}


	/**
	 * Register an array of action hooks for the class.
	 *
	 * @param array $actions. An array of actions in the format:
	 *                        'hook'     => Name of the action hook
	 *                        'callback' => Name of the callback method
	 *                        'priority' => Priority of the action
	 *                        'num_args' => Number of args to send to the callback
	 *
	 * @see add_action() in core for more details.
	 */
	public function register_actions( $actions ) {
		if ( ! is_array( $actions ) || null === $this->object ) {
			return;
		}

		foreach ( $actions as $action ) {
			$action = $this->validate_params( $action );
			if ( $action ) {
				add_action( $action['hook'], array( $this->object, $action['callback'] ), $action['priority'], $action['num_args'] );
			}
		}

	}


	/**
	 * Register an array of filter hooks for the class.
	 *
	 * @param array $filters. An array of actions in the format:
	 *                        'hook'     => Name of the filter hook
	 *                        'callback' => Name of the callback method
	 *
	 * @see add_filter() in core for more details.
	 */
	public function register_filters( $filters ) {
		if ( ! is_array( $filters ) || null === $this->object ) {
			return;
		}

		foreach ( $filters as $filter ) {
			$filter = $this->validate_params( $filter, false );
			if ( $filter ) {
				add_filter( $filter['hook'], array( $this->object, $filter['callback'] ) );
			}
		}

	}


	/**
	 * Helper function to check and sanitize the array values.
	 *
	 * @param array $array The array of action/filter hooks.
	 *
	 * @return mixed false if sanitization fails or sanitized array.
	 */
	private function validate_params( $array, $is_action = true ) {
		if ( ! $this->array_keys_exist( $array ) ) {
			return false;
		}

		return $this->sanitize_hook_values( $array, $is_action );
	}


	/**
	 * Check if the required filter keys are set.
	 *
	 * @param array $array Filter or action hook array.
	 *
	 * @return bool false if required hooks are not present otherwise true.
	 */
	private function array_keys_exist( array $array ) {
		return ( array_key_exists( 'hook', $array ) && array_key_exists( 'callback', $array ) );
	}


	/**
	 * Sanitize the hooks array as they can be overridden.
	 *
	 * @param array $array  Filter or action hook array.
	 * @param bool  $action Whether the array is an action or filter array.
	 *
	 * @return array A sanitized array of values.
	 */
	private function sanitize_hook_values( array $array, $action = true ) {
		$safe_array = array(
			'hook'     => sanitize_text_field( $array['hook'] ),
			'callback' => sanitize_text_field( $array['callback'] ),
		);

		if ( true === $action ) {
			$safe_array['priority'] = isset( $array['priority'] ) ? (int) $array['priority'] : 10;
			$safe_array['num_args'] = isset( $array['num_args'] ) ? (int) $array['num_args'] : 1;
		}

		return $safe_array;
	}

}
