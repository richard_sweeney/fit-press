<?php

if ( ! defined( 'ABSPATH' ) ) return;

class Fit_Press_DB {

	public static $instance;

	/**
	 * @var string $rel_table the relationships table name.
	 */
	public $relationships_table = null;


	/**
	 * @var string $rel_metadata_table the relationship metadata table name.
	 */
	public $relationship_metadata_table = null;


	/** Set the table names. */
	public function __construct() {
		global $wpdb;

		$this->relationships_table = $wpdb->prefix . 'fp_relationships';
		$this->relationship_metadata_table = $wpdb->prefix . 'fp_relationship_meta';
	}


	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.0.1
	 *
	 * @return A single instance of this class.
	 */
	public static function get() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}


	/** Create the required tables for the plugin. */
	public function create_tables() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$thing_one = "CREATE TABLE IF NOT EXISTS $this->relationships_table (
			id bigint(20) unsigned NOT NULL auto_increment,
			item_id bigint(20) unsigned NOT NULL,
			secondary_item_id bigint(20) unsigned NOT NULL,
			user_id bigint(20) unsigned NOT NULL,
			date_recorded datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			PRIMARY KEY (id),
			KEY item_id (item_id),
			KEY secondary_item_id (secondary_item_id)
		);";

		$thing_two = "CREATE TABLE IF NOT EXISTS $this->relationship_metadata_table (
			id bigint(20) unsigned NOT NULL auto_increment,
			relationship_id bigint(20) unsigned NOT NULL,
			meta_key varchar(255) default NULL,
			meta_value longtext,
			PRIMARY KEY (id),
			KEY relationship_id (relationship_id),
			KEY meta_key (meta_key)
		);";

		dbDelta( $thing_one );
		dbDelta( $thing_two );
	}


	/** Drop the tables we created for the plugin. */
	public function drop_tables() {
		global $wpdb;

		$wpdb->query( "DROP TABLE IF EXISTS $this->relationships_table" );
		$wpdb->query( "DROP TABLE IF EXISTS $this->relationship_metadata_table" );
	}


	/**
	 * Get post relationship details for a workout.
	 *
	 * @param int $workout_id Workout post_ID.
	 *
	 * return array Array of results from $wpdb->results call.
	 */
	public function get_workout_relationship_details( $workout_id ) {
		global $wpdb;

		return $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $this->relationships_table WHERE item_id = %d ORDER BY id ASC", $workout_id ) );
	}


	/**
	 * Save a workout to the database.
	 *
	 * @param int   $post_id   Post ID.
	 * @param array $exercises Array of exercises.
	 */
	public function set_workout_exercises( $post_id, $exercises ) {
		global $wpdb;

		foreach ( $exercises as $exercise_id => $exercise ) {
			$wpdb->insert(
				$this->relationships_table,
				array(
					'item_id'           => $post_id,
					'secondary_item_id' => $exercise_id,
					'user_id'           => get_current_user_id(),
					'date_recorded'     => date( 'Y-m-d H:i:s' ),
				),
				array( '%d', '%d', '%d', '%s' )
			);
			$this->set_workout_exercise_metadata( $wpdb->insert_id, $exercise );
		}
	}


	/**
	 * Retrieve a list of exercises for a workout.
	 *
	 * @param int  $workout_id post_id.
	 * @param bool $copy       Whether or not to copy the workout.
	 *
	 * @return $wpdb->query result.
	 */
	public function get_workout_exercises( $workout_id, $copy = false ) {
		$exercises     = array();
		$relationships = $this->get_workout_relationship_details( $workout_id );

		foreach ( $relationships as $relationship ) {
			$exercise = new StdClass;
			$results  = $this->get_workout_exercise_metadata( $relationship->id, 'sets' );

			if ( $results && ! $copy ) {
				$exercise->sets = $results[0];
			} else {
				$exercise->sets = array();
			}

			$exercise->exercise = fit_press_get_exercise( $relationship->secondary_item_id );

			$exercises[] = $exercise;
		}

		return $exercises;
	}


	/**
	 * Retrieve a list of exercise ids for a workout.
	 *
	 * @param int $workout_id post_id.
	 *
	 * @return $wpdb->query result.
	 */
	public function get_workout_exercises_ids( $workout_id ) {
		global $wpdb;

		$ids = $wpdb->get_col( $wpdb->prepare(
			"SELECT id FROM $this->relationships_table WHERE item_id = '%d'", $workout_id
		) );

		return $ids;
	}


	/**
	 * Delete a workout exercises and all related exercise metadata.
	 *
	 * @param int $workout_id Workout post_id.
	 *
	 * @return bool true on success, otherwise false.
	 */
	public function delete_workout_exercises( $workout_id ) {
		global $wpdb;

		$relationship_ids = $this->get_workout_exercises_ids( $workout_id );

		if ( ! $relationship_ids ) {
			return false;
		}
		foreach ( $relationship_ids as $relationship_id ) {
			$result = $wpdb->delete(
				$this->relationship_metadata_table,
				array( 'relationship_id' => $relationship_id ),
				array( '%d' )
			);
		}

		$deleted = $wpdb->delete(
			$this->relationships_table,
			array( 'item_id' => $workout_id ),
			array( '%d' )
		);

		if ( ! $deleted ) {
			return false;
		}

		return true;
	}


	/**
	 * Save exercise metadata to the database.
	 *
	 * @param int   $workout_exercise_id Relationship id.
	 * @param array $exercises           Array of exercise objects.
	 */
	public function set_workout_exercise_metadata( $workout_exercise_id, $exercises ) {
		global $wpdb;

		$wpdb->insert(
			$this->relationship_metadata_table,
			array(
				'relationship_id' => $workout_exercise_id,
				'meta_key'        => 'sets',
				'meta_value'      => serialize( $exercises ),
			),
			array( '%d', '%s', '%s' )
		);

		return $wpdb->insert_id;
	}


	/**
	 * Get exercise metadata.
	 *
	 * @param int         $id  relationship id.
	 * @param bool|string $key optional meta_key to specify in the query.
	 *
	 * @return array $wpdb->query result.
	 */
	public function get_workout_exercise_metadata( $id, $key = false ) {
		global $wpdb;

		if ( $key ) {
			$sql = $wpdb->prepare( "SELECT meta_value FROM $this->relationship_metadata_table WHERE relationship_id = %d AND meta_key = %s", $id, $key );
		} else {
			$sql = $wpdb->prepare( "SELECT meta_value FROM $this->relationship_metadata_table WHERE relationship_id = %d", $id );
		}

		$results = $wpdb->get_col( $sql );
		if ( ! $results ) {
			return false;
		}

		return array_map( 'unserialize', $results );
	}


	/**
	 * Get all instances of an exercise (where it exercise was used in a workout).
	 *
	 * @param int $id exercise id.
	 *
	 * @return array Array of exercise instances.
	 */
	public function get_all_exercise_instances( $id ) {
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT relationships.item_id AS workout_id, relationships.date_recorded AS date, relationship_meta.meta_value AS exercises FROM wp_fp_relationships AS relationships LEFT JOIN wp_fp_relationship_meta AS relationship_meta ON relationships.id = relationship_meta.relationship_id AND relationships.secondary_item_id = %d WHERE relationship_meta.meta_key = 'sets' ORDER BY date DESC" , $id ) );

		$instances = array();
		foreach ( $results as $result ) {
			$result->exercises = maybe_unserialize( $result->exercises );
		}

		return $results;
	}

}

