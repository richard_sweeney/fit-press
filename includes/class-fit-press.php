<?php

if ( ! defined( 'ABSPATH' ) ) return;

include_once( plugin_dir_path( __FILE__ ) . 'fit-press-api.php' );

class Fit_Press {

	public static $instance;

	/**
	 * Constructor function. Register action and filter hooks.
	 */
	private function __construct() {

		$hook_factory = new Fit_Press_Filter_Action_Factory( $this );
		$hook_factory->register_actions( $this->get_actions() );
		$hook_factory->register_filters( $this->get_filters() );

	}


	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.0.1
	 *
	 * @return A single instance of this class.
	 */
	public static function get() {
		if ( self::$instance === null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}


	/**
	 * Register the filter hooks.
	 */
	public function get_actions() {
		return apply_filters( 'fit_press_actions', array(
			array(
				'hook'     => 'admin_menu',
				'callback' => 'register_admin_page',
			),
			array(
				'hook'     => 'admin_enqueue_scripts',
				'callback' => 'enqueue_scripts_and_styles',
			),
			array(
				'hook'     => 'save_post_workout',
				'callback' => 'save_workout',
				'priority' => 10,
				'num_args' => 3,
			),
			array(
				'hook'     => 'wp_ajax_fit-press-create-exercise',
				'callback' => 'create_exercise',
			),
			array(
				'hook'     => 'wp_ajax_fit-press-autosave-workout',
				'callback' => 'autosave_workout',
			),
		) );

	}


	/**
	 * Register the filter hooks.
	 */
	public function get_filters() {
		return apply_filters( 'fit_press_filters', array(
			array(
				'hook'     => 'the_content',
				'callback' => 'add_exercise_data_to_workout',
			),
			array(
				'hook'     => 'the_content',
				'callback' => 'add_exercise_instances_to_exercises',
			),
		) );
	}


	/** Register the admin page */
	public function register_admin_page(){
    	$page_hook_suffix = add_menu_page( 'FitPress', 'FitPress', 'manage_options', 'fitpress/dashboard.php', array( $this, 'render_admin_page' ), '', 100 );

    	add_action( 'admin_print_scripts-' . $page_hook_suffix, array( $this, 'enqueue_admin_page_scripts' ) );
	}


	/** Render the admin page */
	public function render_admin_page() {
		include_once( FITPRESS_DIR . 'views/landing-page.php' );
	}


	/**
	 * Enqueue scripts for FitPress dashboard.
	 */
	public function enqueue_admin_page_scripts() {
		$exercise_instances = fit_press_get_instances_of_exercise( 86 );

		$exercises = array();
		foreach ( $exercise_instances as $exercise ) {
			foreach ( $exercise->exercises as $set ) {
				$exercises[] = array(
					'date'   => strtotime( $exercise->date ),
					'weight' => (int) $set['weight'],
				);
			}
		}
		$t10n = array(
			'exercises' => $exercises
		);

		wp_enqueue_style( 'fit-press-css', plugins_url( 'css/fit-press.css', dirname( __FILE__ ) ) );
		wp_enqueue_script( 'canvasjs', plugins_url( 'js/canvasjs.min.js', dirname( __FILE__ ) ), '0.0.1', true );
		wp_enqueue_script( 'masonry', plugins_url( 'bower_components/masonry/dist/masonry.pkgd.min.js', dirname( __FILE__ ) ), '0.0.1', true );
		wp_enqueue_script( 'fit-press-graphs', plugins_url( 'js/fit-press-graphs.js', dirname( __FILE__ ) ), array( 'jquery', 'masonry', 'canvasjs' ), '0.0.1', true );

		wp_localize_script( 'fit-press-graphs', 'fitPressGraphs', $t10n );
	}


	/** Enqueue JS for the workout page */
	public function enqueue_scripts_and_styles() {
		global $post;

		if ( 'workout' !== get_post_type() ) {
			return;
		}

		$unrequired_scripts = array(
			'wp-fullscreen',
			'colorpicker',
			'editor',
			'cropper',
			'jcrop',
			'swfobject',
			'word-count',
			'media-upload',
			'thickbox',
			'scriptaculous',
			'jcrop',
			'swfobject',
			'plupload',
			'plupload-all',
			'plupload-html5',
			'plupload-flash',
			'plupload-silverlight',
			'plupload-html4',
			'plupload-handlers',
			'wp-plupload',
			'swfupload',
			'swfupload-swfobject',
			'swfupload-queue',
			'swfupload-speed',
			'swfupload-all',
			'swfupload-handlers',
			'comment-reply',
			'imageareaselect',
			'mediaelement',
			'wp-mediaelement',
			'wp-playlist',
			'password-strength-meter',
			'word-count',
			'media-upload',
			'shortcode',
			'media-models',
			'wp-backbone',
			'media-views',
		);

		// Get rid of a TON of unnecessary scripts so we can keep our
		// app nice and snappy.
		foreach ( $unrequired_scripts as $script ) {
			wp_deregister_script( $script );
		}

		$workout   = fit_press_get_workout_exercises();
		$exercises = fit_press_get_exercises();
		$areas     = get_terms( 'body_part' );

		foreach ( $exercises as $exercise ) {
			$curr_exercise       = new \StdClass;
			$curr_exercise->id   = $exercise->ID;
			$curr_exercise->name = $exercise->post_title;
			$curr_exercise->area = wp_get_post_terms( $exercise->ID, 'body_part', array( 'fields' => 'names' ) );
			$angular_exercises[] = $curr_exercise;
		}

		$l10n = array(
			'workout'     => $workout,
			'exercises'   => $angular_exercises,
			'areas'       => $areas,
			'hideAll'     => __( 'Hide all', 'fit_press' ),
			'showAll'     => __( 'Show all', 'fit_press' ),
			'newExercise' => __( 'New Exercise', 'fit_press' ),
			'ajaxNonce'   => wp_create_nonce( 'fit-press-nonce' ),
		);

		wp_enqueue_style( 'fit-press-css', plugins_url( 'css/fit-press.css', dirname( __FILE__ ) ) );
		wp_enqueue_script( 'fit-press-js', plugins_url( 'js/fit-press-bb.js', dirname( __FILE__ ) ), array( 'backbone', 'underscore' ), '0.0.1', true );

		wp_localize_script( 'fit-press-js', 'fitPress', $l10n );
	}


	/**
	 * Save the workout data.
	 *
	 * @param int     $post_ID Post ID.
	 * @param WP_Post $post    Post object.
	 * @param bool    $update  Whether this is an existing post being updated or not.
	 */
	public function save_workout( $post_id, $post, $update ) {
		if ( ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || ( defined('DOING_AJAX') && DOING_AJAX ) ) {
        	return;
        }

		if ( isset( $_POST['fitpress'] ) ) {
			fit_press_save_workout( $_POST['fitpress'], $post_id, $update );
		}

	}


	/**
	 * Append workout data to a single workout.
	 *
	 * @pararm string $content Post content.
	 *
	 * return string Post content with appended exercise data.
	 */
	public function add_exercise_data_to_workout( $content ) {
		if ( is_singular( 'workout' ) ) {
			$exercises = fit_press_get_workout_exercises();
			$extra_content = '';
			foreach ( $exercises as $exercise ) :
				$extra_content .= sprintf(
					'<h4 class="fit-press-exercise-name"><a href="%s">%s</a></h4>' . "\n",
					get_permalink( $exercise->exercise->ID ),
					apply_filters( 'the_title', $exercise->exercise->post_title )
				);
				$extra_content .= '<table>' . "\n";
					$extra_content .= '<thead>' . "\n";
						$extra_content .= '<tr>' . "\n";
							$extra_content .= '<th>' . __( 'Weight', 'fit_press' ) . '</th>' . "\n";
							$extra_content .= '<th>' . __( 'Reps', 'fit_press' ) . '</th>' . "\n";
						$extra_content .= '</tr>' . "\n";
					$extra_content .= '</thead>' . "\n";
					$extra_content .= '<tbody>' . "\n";
					foreach ( $exercise->sets as $set ) :
						$extra_content .= '<tr>' . "\n";
							$extra_content .= "<td>{$set['weight']} kg </td>\n";
							$extra_content .= "<td>{$set['reps']}</td>\n";
						$extra_content .= '</tr>' . "\n";
					endforeach;
					$extra_content .= '</tbody>' . "\n";
				$extra_content .= '</table>' . "\n";
			endforeach;
			$content = $content . $extra_content;
		}

		return $content;
	}


	/**
	 * Append exercise data to a single exercise.
	 *
	 * @pararm string $content Post content.
	 *
	 * return string Post content with appended exercise data.
	 */
	public function add_exercise_instances_to_exercises( $content ) {
		global $post;
		if ( ! is_singular( 'exercise' ) ) {
			return $content;
		}

		$exercise_instances = fit_press_get_instances_of_exercise( $post->ID );
		$extra_content      = '';
		$weight             = array();

		foreach ( $exercise_instances as $exercise ) :
			$extra_content .= sprintf(
				'<h4 class="fit-press-exercise-name"><a href="%s">%s</a></h4>' . "\n",
				get_permalink( $exercise->workout_id ),
				get_the_title( $exercise->workout_id )
			);

			$extra_content .= '<table>' . "\n";
				$extra_content .= '<thead>' . "\n";
					$extra_content .= '<tr>' . "\n";
						$extra_content .= '<th>' . __( 'Weight', 'fit_press' ) . '</th>' . "\n";
						$extra_content .= '<th>' . __( 'Reps', 'fit_press' ) . '</th>' . "\n";
					$extra_content .= '</tr>' . "\n";
				$extra_content .= '</thead>' . "\n";
				$extra_content .= '<tbody>' . "\n";
				foreach ( $exercise->exercises as $set ) :
					$weight[] = $set['weight'];
					$extra_content .= '<tr>' . "\n";
						$extra_content .= "<td>{$set['weight']} kg </td>\n";
						$extra_content .= "<td>{$set['reps']}</td>\n";
					$extra_content .= '</tr>' . "\n";
				endforeach;
				$extra_content .= '</tbody>' . "\n";
			$extra_content .= '</table>' . "\n";
		endforeach;


		if ( ! empty( $weight ) ) :
			$extra_content .= sprintf(
				'<p>%s: %s kg. <strong>%s</strong>.</p>',
				__( 'Heaviest lift to date', 'fit_press' ),
				max( $weight ),
				__( 'Awesome', 'fit_press' )
			);
		endif;

		return $content . $extra_content;
	}


	/**
	 * Add an exercise to the database when adding a workout.
	 *
	 * @return wp_send_json_error() or wp_send_json_success().
	 */
	public function create_exercise() {
		check_ajax_referer( 'fit-press-nonce', 'security' );

		$title = sanitize_text_field( $_POST['title'] );

		$id = wp_insert_post( array(
			'post_type'   => 'exercise',
			'post_title'  => $title,
			'post_status' => 'publish',
			'post_author' => get_current_user_id(),
		) );

		if ( is_wp_error( $id ) ) {
			wp_send_json_error();
		}

		$term_ids = array_map( 'absint', $_POST['term_ids'] );
		wp_set_object_terms( $id, $term_ids, 'body_part' );

		$exercise       = new \StdClass;
		$exercise->id   = $id;
		$exercise->name = $title;
		$exercise->area = wp_get_post_terms( $id, 'body_part', array( 'fields' => 'names' ) );

		wp_send_json_success( $exercise );
	}


	/**
	 * Autosave a workout.
	 *
	 * @return wp_send_json_success().
	 */
	public function autosave_workout() {
		check_ajax_referer( 'fit-press-nonce', 'security' );

		$post_id     = (int) $_POST['id'];
		$post_status = get_post_status( $post_id );

		// Always publish new workouts. This may or may not be a good idea...
		// if ( 'draft' === $post_status ) {
		// 	wp_update_post( array(
		// 		'ID'          => $post_id,
		// 		'post_status' => 'publish'
		// 	) );
		// }

		fit_press_save_workout( $_POST['exercises'], $post_id, true );
		wp_send_json_success();
	}


}
