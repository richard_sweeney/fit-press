<?php

if ( ! defined( 'ABSPATH' ) ) return;

class Fit_Press_Workout {

	/**
	 * @var int Workout ID
	 */
	public $ID = null;

	/**
	 * @var string Workout Date
	 */
	public $date = null;

	/**
	 * @var array Array of Workout Exercises
	 */
	public $exercises = null;


	public function __construct( $workout_id ) {
		$this->ID = (int) $workout_id;

		$this->date = date_i18n( 'j F Y, H:i', get_the_time( 'U', $this->ID ) );

		$this->exercises = fit_press_get_workout_exercises( $workout_id );

		return $this;
	}

}
