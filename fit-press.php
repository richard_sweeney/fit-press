<?php
/*
Plugin Name: FitPress
Plugin URI: http://richardsweeney.com/
Description: A workout log for WordPress
Text Domain: fit_press
Domain Path: /languages
Version: 0.0.3
Author: Richard Sweeney
Author URI: http://richardsweeney.com/
License: GPLv2
Bitbucket Plugin URI: https://bitbucket.org/richard_sweeney/fitness-presser
Bitbucket Branch: master
*/

if ( ! defined( 'ABSPATH' ) ) return;

define( 'FITPRESS_VERSION', '0.0.3' );
define( 'FITPRESS_DIR', plugin_dir_path( __FILE__ ) );
define( 'FITPRESS_URL', plugin_dir_url( __FILE__ ) );

// Include the API.
include_once( plugin_dir_path( __FILE__ ) . 'includes/fit-press-api.php' );


/**
 * Class Autoloader. Reformats class name to match WP's coding
 * standards, eg: Name_Of_The_Class = class-name-of-the-class.php
 *
 * @param string $classname Name of the class.
 */
function fitpress_autoloader( $classname ) {
	$classname = explode( '\\', $classname );
    $classfile = sprintf( '%sincludes/class-%s.php',
        plugin_dir_path( __FILE__ ),
        str_replace( '_', '-', strtolower( end( $classname ) ) )
    );
    if ( file_exists( $classfile ) ) {
    	include_once( $classfile );
    }
}
spl_autoload_register( 'fitpress_autoloader' );

// Instantiate all the things!

Fit_Press::get();
Fit_Press_DB::get();
Fit_Press_Post_Type_Workout::get();
Fit_Press_Post_Type_Exercise::get();


/** Load translations */
load_plugin_textdomain( 'fit_press', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );


/** Setup our activation and deactivation hooks */
register_activation_hook(   __FILE__, 'fit_press_activate'   );
register_deactivation_hook( __FILE__, 'fit_press_deactivate' );
register_uninstall_hook(    __FILE__, 'fit_press_uninstall'  );


/**
 * Activation hook for the plugin.
 * @since  0.0.1
 */
function fit_press_activate() {
	$db = new Fit_Press_DB;
	$db->create_tables();

	add_option( 'fit_press_db_version', FITPRESS_VERSION );

	flush_rewrite_rules( true );
}


/**
 * Deactivation hook for the plugin.
 * @since  0.0.1
 */
function fit_press_deactivate() {
	delete_option( 'fit_press_db_version' );

	flush_rewrite_rules( true );
}


/**
 * Uninstall hook for the plugin.
 * @since  0.0.1
 */
function fit_press_uninstall() {
	$db = new Fit_Press_DB;
	$db->drop_tables();

	delete_option( 'fit_press_db_version' );
	flush_rewrite_rules( true );
}

