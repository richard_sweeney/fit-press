module.exports = function( grunt ) {
    'use strict';

    // Load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Project configuration
    grunt.initConfig( {
        pkg:    grunt.file.readJSON( 'package.json' ),

        jshint: {
            options: {
                onecase: false
            },
            all: ['js/fit-press-bb.js', 'js/fit-press-graphs.js']
        },

        compass: {
            dist: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'css',
                    imagesDir: 'css/img',
                    outputStyle: 'compact',
                    noLineComments: true
                }
            }
        },

        watch:  {

            scripts: {
                files: ['js/fit-press-bb.js'],
                tasks: ['jshint'],
                options: {
                    debounceDelay: 200
                }
            },

            livereload: {
                options: { livereload: true },
                files: ['css/*'],
            },

            css: {
                files: ['sass/*.scss'],
                tasks: ['compass'],
            }

        }

    } );

    // Default task.
    grunt.registerTask( 'default', ['jshint', 'compass'] );

    grunt.util.linefeed = '\n';
};
