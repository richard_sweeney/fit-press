<?php global $post; ?>
<div class="postbox fitpress-postbox">

	<div class="inside">

		<h2><?php _e( 'Workout Exercises', 'fit_press' ) ?></h2>

		<div id="fit-press-exercise-select" data-post-id="<?php echo $post->ID; ?>">
			<div id="fit-press-area-selector" ></div>
			<div id="fit-press-new-exercise-container" class="fit-press-new-exercise-container">
				<h4><?php _e( 'Add new exercise', 'fit_press' ) ?> • <a href="#" class="fit-press-new-exercise-cancel" id="fit-press-new-exercise-cancel"><?php _e( 'cancel', 'fit_press' ) ?></a></h4>
				<input type="text" name="fitpress-new-exercise" id="fit-press-new-exercise" placeholder="<?php esc_attr_e( 'Exercise title', 'fit_press' ) ?>" />
				<ul id="body-part-terms-list" class="body-part-terms-list"></ul>

				<div class="clearfix">
					<a href="#" class="button button-secondary create-exercise" id="fit-press-new-exercise-submit"><?php _e( 'Create Exercise', 'fit_press' ) ?></a>
					<span class="spinner" id="new-exercise-spinner"></span>
				</div>
			</div>

			<ul id="fit-press-exercises"></ul>
		</div>

		<div id="fit-press-container"></div>

	</div>

</div>


<script type="text/template" id="sets-template">

	<div class="set-container">
		<h4 class="fit-press-exercise-header"><%= exerciseName %> • <a href="#" class="submitdelete remove-exercise"><?php _e( 'Remove Exercise', 'fit_press' ) ?></a></h4>
		<ul class="fit-press-exercise-list" id="sets-container"></ul>

		<a class="button button-primary copy-set" href="#"><?php _e( 'Copy Set', 'fit_press' ); ?></a>
		<a class="button button-secondary add-set" href="#"><?php _e( 'Add Set', 'fit_press' ); ?></a>
		<a class="button button-secondary new-exercise" href="#"><?php _e( 'New Exercise', 'fit_press' ); ?></a>
	</div>

</script>

<script type="text/template" id="set-template">

	<input class="fit-press-weight" type="number" min="0" name="fitpress[<%= exerciseId %>][<%= index %>][weight]" value="<%= model.weight %>"> kg
	x
	<input class="fit-press-reps" type="number" min="0" name="fitpress[<%= exerciseId %>][<%= index %>][reps]" value="<%= model.reps %>" class="reps-input">
	<a class="remove-set" href="#"><?php _e( 'Remove', 'fit_press' ); ?></a>

</script>
