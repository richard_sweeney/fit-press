<div class="wrap fitpress">

	<h2><?php _e( 'FitPress', 'fit_press' ) ?></h2>

 	<a class="button button-secondary" href="<?php echo admin_url( 'post-new.php?post_type=workout' ); ?>"><?php _e( 'Add a new workout', 'fit_press' ); ?></a>

	<h3><?php _e( 'Recent Workouts', 'fit_press' ) ?></h3>

	<div class="fp-recent-workouts" id="fp-recent-workouts">

		<?php
			$workouts = fit_press_get_workouts( array( 'posts_per_page' => '7' ) );
			if ( $workouts->have_posts() ) :
				while ( $workouts->have_posts() ) :
					$workouts->the_post();
					$exercises = fit_press_get_workout_exercises( get_the_ID() ) ?>
					<article class="fitpress-workout">
						<header class="fp-workout-title">
							<a href="<?php the_permalink() ?>">
								<h4><?php the_title() ?></h4>
								<time datetime="<?php the_time( 'Y-m-d H:i:s' ) ?>"><?php the_time( get_option( 'date_format' ) ) ?></time>
							</a>
						</header>
						<?php foreach ( $exercises as $exercise ) : ?>
							<h4 class="fit-press-exercise-name">
								<a href="<?php echo get_permalink( $exercise->exercise->ID ) ?>">
									<?php echo apply_filters( 'the_title', $exercise->exercise->post_title ) ?>
								</a>
							</h4>
							<table>
								<thead>
									<tr>
										<th><?php _e( 'Weight', 'fit_press' ) ?></th>
										<th><?php _e( 'Reps', 'fit_press' ) ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $exercise->sets as $set ) : ?>
										<tr>
											<td><?php echo esc_html( $set['weight'] ) ?> kg </td>
											<td><?php echo esc_html( $set['reps'] ) ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php endforeach; ?>
					</article>

				<?php endwhile;
			else : ?>

			<p><?php _e( 'There are no workouts to display', 'fit_press' ) ?></p>

		<?php endif; ?>

	</div>

	<div id="chart-container" style="height: 300px; width: 100%;">

</div>
